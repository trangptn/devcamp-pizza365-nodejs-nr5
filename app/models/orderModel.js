const mongoose = require("mongoose");

const Schema= mongoose.Schema;

const orderSchema= new Schema({
    __id: mongoose.Types.ObjectId,
    kichCo:
    {
        type:String,
        required:true
    },
    duongKinh: {
       type:String,
       required:true
    },
    suon:{
        type:String,
        required:true
    },
    salad: {
        type:String,
        required:true
    },
    loaiPizza: {
        type:String,
        required:true
    },
    idVourcher: {
        type:String,
        required:true
    },
    idLoaiNuocUong: {
        type:String,
        required:true
    },
    soLuongNuoc:{
        type:String,
        required: true
    },
    hoTen:{
        type:String,
        required:true
    },
    thanhTien:{
        type:Number,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    soDienThoai:{
        type:String,
        required:true
    },
    diaChi: {
        type:String,
        required:true
    },
    loiNhan:{
        type:String
    },
    trangThai:{
        type:String,
    }
},
{
    timestamps:true
}
);

module.exports=mongoose.model("order",orderSchema);