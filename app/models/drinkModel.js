const mongoose=require("mongoose");
const schema= mongoose.Schema;
const newDrinkSchema=new schema({
    __id:mongoose.Types.ObjectId,
    maNuocUong:{
        type:String,
        unique:true,
        required:true
    },
    tenNuocUong:{
        type:String,
        required:true
    },
    donGia:{
        type:Number,
        required:true
    },
},
{
    timestamps:true
})
module.exports=mongoose.model("drink",newDrinkSchema);