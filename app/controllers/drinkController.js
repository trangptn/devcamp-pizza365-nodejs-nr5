const drinkModel= require('../models/drinkModel');

const mongoose= require('mongoose');

const createDrink = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        manuocuong,
        tennuocuong,
        dongia
    } = req.body;

    // B2: Validate du lieu

    if(dongia < 0) {
        return res.status(400).json({
            message: "Don gia khong hop le"
        })
    }

    try {
        // B3: Xu ly du lieu

        var newDrink = {
            maNuocUong:manuocuong,
            tenNuocUong:tennuocuong,
            donGia:dongia
        }
        const result = await drinkModel.create(newDrink);
        return res.status(201).json({
            message: "Tao drink thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra"
           
        })
    }
}

const getAllDrinks = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await drinkModel.find();

        return res.status(200).json({
            message: "Lay danh sach drink thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getDrinkById = async (req, res) => {
    // B1: Thu thap du lieu
    const drinkid = req.params.drinkid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            message: "Drink ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await drinkModel.findById(drinkid);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin drink thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin drink"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


const updateDrinkById = async (req, res) => {
    // B1: Thu thap du lieu
    const drinkid = req.params.drinkid;
    const {
        tennuocuong,
        dongia
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            message: "Drink ID khong hop le"
        })
    }

    if(dongia < 0) {
        return res.status(400).json({
            message: "Don gia khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateDrink = {};
        if(tennuocuong) {
            newUpdateDrink.tenNuocUong = tennuocuong
        }
        if(dongia) {
            newUpdateDrink.donGia = dongia
        }

        const result = await drinkModel.findByIdAndUpdate(drinkid, newUpdateDrink);

        if(result) {
            return res.status(200).json({
                message: "Update thong tin drink thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin drink"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteDrinkById = async (req, res) => {
    // B1: Thu thap du lieu
    const drinkid = req.params.drinkid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            message: "Drink ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await drinkModel.findByIdAndRemove(drinkid);

        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin drink thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin drink"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


module.exports={ createDrink, getAllDrinks, getDrinkById, updateDrinkById,deleteDrinkById};

