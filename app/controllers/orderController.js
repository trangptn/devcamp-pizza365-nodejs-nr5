const mongoose = require('mongoose');
const orderModel = require('../models/orderModel');
// const userModel = require('../models/userModel');

const createOrder = (req, res) => {
    //B1:Thu thap du lieu
    const {
        reqKichCo,
        reqDuongKinh,
        reqSuon,
        reqSalad,
        reqLoaiPizza,
        reqIdVourcher,
        reqIdLoaiNuocUong,
        reqSoLuongNuoc,
        reqHoTen,
        reqThanhTien,
        reqEmail,
        reqSoDienThoai,
        reqDiaChi,
        reqLoiNhan,
        reqtrangThai } = req.body;
    //B2:Validate data
    if (!reqDiaChi) {
        res.status(400).json({
            message: "Dia chi khong hop le"
        })
        return false;
    }
    if (!reqDuongKinh) {
        res.status(400).json({
            message: "Duong kinh khong hop le"
        })
        return false;
    }
    if (!reqEmail) {
        res.status(400).json({
            message: "Email khong hop le"
        })
        return false;
    }
    if (!reqHoTen) {
        res.status(400).json({
            message: "Ho ten khong hop le"
        })
        return false;
    }
    if (!reqIdLoaiNuocUong) {
        res.status(400).json({
            message: "Id nuoc khong hop le"
        })
        return false;
    }
    if (!reqIdVourcher) {
        res.status(400).json({
            message: "Id voucher khong hop le"
        })
        return false;
    }
    if (!reqKichCo) {
        res.status(400).json({
            message: "Kich co khong hop le"
        })
        return false;
    }
    if (!reqLoaiPizza) {
        res.status(400).json({
            message: "Loai pizza khong hop le"
        })
        return false;
    }
    if (!reqSalad) {
        res.status(400).json({
            message: "Salad khong hop le"
        })
        return false;
    }
    if (!reqSoDienThoai) {
        res.status(400).json({
            message: "So dien thoai khong hop le"
        })
        return false;
    }
    if (!reqSoLuongNuoc) {
        res.status(400).json({
            message: "So luong nuoc khong hop le"
        })
        return false;
    }
    if (!reqThanhTien) {
        res.status(400).json({
            message: "Thanh tien khong hop le"
        })
        return false;
    }
    if (!reqSuon) {
        res.status(400).json({
            message: "Suon khong hop le"
        })
        return false;
    }
    //B3:Xu ly va tra ve ket qua
    let newOrder = new orderModel({
        kichCo:reqKichCo,
        duongKinh:reqDuongKinh,
        suon:reqSuon,
        salad:reqSalad,
        loaiPizza:reqLoaiPizza,
        idVourcher:reqIdVourcher,
        idLoaiNuocUong:reqIdLoaiNuocUong,
        soLuongNuoc:reqSoLuongNuoc,
        hoTen:reqHoTen,
        thanhTien:reqThanhTien,
        email:reqEmail,
        soDienThoai:reqSoDienThoai,
        diaChi:reqDiaChi,
        loiNhan:reqLoiNhan,
        trangThai:reqtrangThai
    })

    orderModel.create(newOrder)
            .then((data) => {
                res.status(201).json({
                    message: "Successfully created",
                    result:data
                })
            })
                .catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        message: `Internal sever error: ${err.message}`
                    })
                })
}

const getAllOrder=(req,res) =>{
    //B1: Thu thap du lieu
    //B2:validate data
    //B3: Xu ly va tra ve ket qua
    orderModel.find().exec()
        .then(data=> {
            return res.status(200).json({
                message:"Successfully load all data!",
                order: data
            })
        })
        .catch(error =>{
            return res.status(500).json({
                message:`Internal sever error: ${error.message}`
            })
        })
}
const getOrderById=(req,res)=>{
    //B1: Thu thap du lieu
    var orderId=req.params.orderId;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(orderId))
   {
       return res.status(400).json({
           message:"Id is invalid!"
       })
   }
   //B3: Xu ly va tra ve ket qua
   orderModel.findById(orderId)
       .then(data=>{
           if(!data)
           {
               return res.status(404).json({
                   message:" Order not found"
               })
           }
          return res.status(200).json({
           message:"Successfully load data by ID!",
           order: data
          })
       })
       .catch(err=>{
           return res.status(500).json({
               message:`Internal sever error: ${err.message}`
           })
       })
}
const updateOrderById= async (req,res)=>{
    //B1: Thu thap du lieu
    var orderId=req.params.orderId;
    const {
        reqKichCo,
        reqDuongKinh,
        reqSuon,
        reqSalad,
        reqLoaiPizza,
        reqIdVourcher,
        reqIdLoaiNuocUong,
        reqSoLuongNuoc,
        reqHoTen,
        reqThanhTien,
        reqEmail,
        reqSoDienThoai,
        reqDiaChi,
        reqLoiNhan,
        reqtrangThai} = req.body;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(orderId))
   {
       return res.status(400).json({
           message:"Id is invalid!"
       })
   }
   if (reqDiaChi=="") {
    res.status(400).json({
        message: "Dia chi khong hop le"
    })
    return false;
}
if (reqDuongKinh=="") {
    res.status(400).json({
        message: "Duong kinh khong hop le"
    })
    return false;
}
if (reqEmail=="") {
    res.status(400).json({
        message: "Email khong hop le"
    })
    return false;
}
if (reqHoTen=="") {
    res.status(400).json({
        message: "Ho ten khong hop le"
    })
    return false;
}
if (reqIdLoaiNuocUong=="") {
    res.status(400).json({
        message: "Id nuoc khong hop le"
    })
    return false;
}
if (reqIdVourcher=="") {
    res.status(400).json({
        message: "Id voucher khong hop le"
    })
    return false;
}
if (reqKichCo=="") {
    res.status(400).json({
        message: "Kich co khong hop le"
    })
    return false;
}
if (reqLoaiPizza=="") {
    res.status(400).json({
        message: "Loai pizza khong hop le"
    })
    return false;
}
if (reqSalad=="") {
    res.status(400).json({
        message: "Salad khong hop le"
    })
    return false;
}
if (reqSoDienThoai=="") {
    res.status(400).json({
        message: "So dien thoai khong hop le"
    })
    return false;
}
if (reqSoLuongNuoc=="") {
    res.status(400).json({
        message: "So luong nuoc khong hop le"
    })
    return false;
}
if (reqThanhTien=="") {
    res.status(400).json({
        message: "Thanh tien khong hop le"
    })
    return false;
}
if (reqSuon=="") {
    res.status(400).json({
        message: "Suon khong hop le"
    })
    return false;
}
   //B3: Xu ly va tra ve ket qua
  try{
    let updateOrder={
        kichCo:reqKichCo,
        duongKinh:reqDuongKinh,
        suon:reqSuon,
        salad:reqSalad,
        loaiPizza:reqLoaiPizza,
        idVourcher:reqIdVourcher,
        idLoaiNuocUong:reqIdLoaiNuocUong,
        soLuongNuoc:reqSoLuongNuoc,
        hoTen:reqHoTen,
        thanhTien:reqThanhTien,
        email:reqEmail,
        soDienThoai:reqSoDienThoai,
        diaChi:reqDiaChi,
        loiNhan:reqLoiNhan,
        trangThai:reqtrangThai
    }
    const updateO= await orderModel.findByIdAndUpdate(orderId,updateOrder);
    if(updateO){
        return res.status(200).json({
            message:"Successfully update data by ID!",
            data: updateO
    })
  }
  else{
        return res.status(404).json({
        message:" Order not found"
        })
  }
}catch(err){
           return res.status(500).json({
               message:`Internal sever error: ${err.message}`
           })
       }
}
const deleteOrderById= async (req,res)=>{
    // B1: Thu thap du lieu
    const orderId = req.params.orderId;

    var userId=req.query.userId;


    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Order ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const deleteOrder= await  orderModel.findByIdAndDelete(orderId)
        if(userId != undefined){
          await  userModel.findByIdAndUpdate(userId,{
                $pull: {orders: orderId}})
        }

        if(deleteOrder){
            return res.status(200).json({
                message:"Successfully delete data by ID!",
                data: deleteOrder
               })
        }
        else{
            return res.status(404).json({
                message:" Order not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
        message:`Internal sever error: ${error.message}`
        })
    }
}

// const orderHandle=(req,res)=>{
//    const email=req.body.email;
//    const random=Math.floor(Math.random()*100);
//    userModel.findOne({email:email})
//    .then((exitEmail)=>{
//         if(exitEmail)
//         {
//             orderModel.create({
//                 _id: new mongoose.Types.ObjectId(),
//                 orderCode:random,
//                 pizzaSize:" ",
//                 pizzaType:" ",
//                 status:" "
//             })
//             .then((createOrder)=>{
//                 res.status(201).json({
//                     data:createOrder
//                 })
//             })
//             .catch((errCreated)=>{
//                 console.log(errCreated);
//                 res.status(500).json({
//                     message:"Loi tao order"
//                 })
//             })
//         }
//         else{
//             userModel.create({
//                 _id: new mongoose.Types.ObjectId(),
//                 email:email,
//                 fullName:" ",
//                 address:" ",
//                 phone:" "
//             })
//             .then((userCreat)=>{
//                 orderModel.create({
//                     _id:userCreat._id,
//                     orderCode:random,
//                     pizzaSize:" ",
//                     pizzaType:" ",
//                     status:" "
//                 })
//                 .then(()=>{
//                     res.status(201).json({
//                         data:userCreat
//                     })
//                 })
//                 .catch((errOrder)=>{
//                     res.status(500).json({
//                         message:"Loi tao order"
//                     })
//                 })
//             })
//             .catch((errUser)=>{
//                 res.status(500).json({
//                     message:"Loi tao user"
//                 })
//             })
//         }
//    })
//    .catch((err)=>{
//     console.log(err);
//     res.status(500).json({
//         message:"Loi "
//     })
//    })
// }
module.exports={
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    // orderHandle
}