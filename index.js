//import thu vien express
const express = require('express');

const mongoose=require('mongoose');
const port= 8000;

mongoose.connect("mongodb://127.0.0.1:27017/NR5")
.then(() =>{
    console.log("Connect mongoDB Successfully");
})
.catch((error) =>{
    console.log(error);
});

const app= new express();
app.use(express.json());

const voucherRouter=require("./app/router/voucherRouter");
const orderRouter=require("./app/router/orderRouter");
const drinkRouter=require("./app/router/drinkRouter");



app.use("/",voucherRouter);
app.use("/",orderRouter);
app.use("/",drinkRouter);

app.listen(port, () => {
    console.log(`App  listening  on port ${port}`);
})